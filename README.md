# Grót - A collection of building blocks.

## Getting started

Grót is a collection of [react](https://www.npmjs.com/package/react) building blocks, which are heavily dependent on [react-bootstrap](https://www.npmjs.com/package/react-bootstrap).

## Example

Demonstration and documentation of the different components can be found on
[johandavidsen.bitbucket.org/Grot](http://johandavidsen.bitbucket.org/Grot).

## Install

    npm install grot

## Usage

The different components can be imported into any React class or component. The examples below demonstrate how.

    import { GrotHello } from 'grot';

or

    var GrotHello = require('grot').GrotHello;

As mentioned above this library builds on the library [react-bootstrap](https://www.npmjs.com/package/react-bootstrap) and as such the library needs the same CSS files as [react-bootstrap](https://www.npmjs.com/package/react-bootstrap) (read more on there webpage). In addition to the css files for [react-bootstrap](https://www.npmjs.com/package/react-bootstrap), I have compiled a custom CSS file for my components. You can choose to use this if you want, the file is stored in the location:

    lib\style

<!-- ## Changelog -->

<!-- ## License -->
